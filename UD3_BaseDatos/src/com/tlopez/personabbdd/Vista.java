package com.tlopez.personabbdd;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * @author Tania
 * @since 13-12-2019
 * Clase Vista
 */
public class Vista {
    //Campos
    private JPanel panel1;
     JTextField txtNombre;
     JTextField txtApellido;
     JTextField txtDireccion;
     JTextField txtDni;
     JTextField txtTelefono;
     JButton btnNuevo;
     JButton btnEliminar;
     JButton btnBuscar;
     JTextField txtBuscar;
     DatePicker txtFechaNacimineto;
     JTable tabla;
     JLabel lblAccion;
     JButton btnCrearTablaNueva;
     JTextField txtTabla;
     JButton btnEliminarTodo;
    DefaultTableModel dtm;

    JMenuItem itemConectar;
    JMenuItem itemSalir;
    JFrame frame;
    JMenuItem itemCrearTablaPersona;

    /**
     * Constructor de Vista sin parametros
     */
    public Vista(){
        //Ventana
        frame = new JFrame("Carnet de identidad");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Inicializo el DefaultTableModel
        dtm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                if(column == 0){
                    return false;
                }
                return true;
            }
        };

        tabla.setModel(dtm);
        //Menu
        menu();
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }

    /**
     * Metodo menu(), donde creo un despegable Archivo que contiene los items Conectar y Salir
     */
    private void menu(){
        //items
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        itemCrearTablaPersona = new JMenuItem("Crear tabla persona");
        itemCrearTablaPersona.setActionCommand("Crear Tabla Personas");
        //JMenu
        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);
        menuArchivo.add(itemCrearTablaPersona);
        //Barra de menu
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);
        //Agrego a la ventana
        frame.setJMenuBar(barraMenu);
    }

}

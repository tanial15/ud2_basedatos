package com.tlopez.personabbdd;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
/**
 * @author Tania
 * @since 13-12-2019
 * Clase Controlador
 * @see java.awt.event.ActionListener
 * @see javax.swing.event.TableModelListener
 */
public class Controlador implements ActionListener, TableModelListener {
    //Campos
    private Vista vista;
    private Modelo modelo;

    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;

    /**
     * Constructor  del Controlador
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        this.estado = tipoEstado.desconectado;
        //Inicio tabla
        iniciarTabla();
        //Anyado los listeners
        addActionListener(this);
        addTableModelListeners(this);

    }

    /**
     * Metodo addTableModelListeners() donde le anyado el listener a la tabla
     * @param listener
     */
    private void addTableModelListeners(TableModelListener listener) {
        vista.dtm.addTableModelListener(listener);
    }

    /**
     * Metodo addActionListener() donde le anyado los listeners  a los botones y a los items
     * @param listener
     */
    private void addActionListener(ActionListener listener) {
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnCrearTablaNueva.addActionListener(listener);
        vista.itemCrearTablaPersona.addActionListener(listener);
        vista.btnEliminarTodo.addActionListener(listener);
    }

    /**
     * Metodo implementado por ActionListener actionPerformed(), donde llamo a todos los metodos de la base de datos(Modelo)
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Crear Tabla ":
                try {
                    modelo.crearTabla(vista.txtTabla.getText());

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                limpiarCampos();
                vista.lblAccion.setText("Tabla nueva creada");

                break;
            case "Nuevo":
                try {
                    modelo.insertarPersona(vista.txtNombre.getText(),vista.txtApellido.getText(),
                            vista.txtFechaNacimineto.getDate(), vista.txtDireccion.getText(),vista.txtDni.getText(),
                            Integer.parseInt(vista.txtTelefono.getText()));
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                limpiarCampos();
                break;
            case "Crear Tabla Personas":
                try {
                   modelo.crearTablaPersonas();
                    vista.lblAccion.setText("Tabla persona creada");
                    vista.lblAccion.setText("Tabla creada en la base de datos");

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Buscar":
                try {
                    if(vista.txtBuscar.getText().equalsIgnoreCase("")) {
                        cargarFilas(modelo.buscarDatos(""));
                    }
                    else {
                        cargarFilas(modelo.buscarDatos(vista.txtBuscar.getText()));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                limpiarCampos();
                vista.lblAccion.setText("Resultados Encontrados");
                break;
            case "Eliminar":
                try {
                    int filaBorrar = vista.tabla.getSelectedRow();
                    int idBorrar = (Integer)vista.dtm.getValueAt(filaBorrar,0);
                    modelo.eliminarPersona(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Vaciar tabla":
                try {
                    modelo.vaciarTabla();
                    int numDatos = vista.dtm.getRowCount();
                    for (int i = 0; i < numDatos; i++) {
                        vista.dtm.removeRow(0);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Salir":
                System.exit(0);
                break;
            case "Conectar":
                if(estado == tipoEstado.desconectado) {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado = tipoEstado.conectado;
                        cargarFilas(modelo.obtenerDatos());

                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de conexion","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                }else{
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado = tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de desconexion","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                }
                break;
        }
    }

    /**
     * Metodo limpiarCampos() para limpiar los campos
     */
    private void limpiarCampos() {
        vista.txtNombre.setText("");
        vista.txtApellido.setText("");
        vista.txtFechaNacimineto.setText("");
        vista.txtDireccion.setText("");
        vista.txtDni.setText("");
        vista.txtTelefono.setText("");
        vista.txtBuscar.setText("");
        vista.txtTabla.setText("");

    }

    /**
     * Metodo implementado por TableModelListener , para que actualice la tabla cuando es modificada
     * @param e
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if(e.getType() == TableModelEvent.UPDATE){
            System.out.println("actualizada");
            int filaModificada = e.getFirstRow();
            try {
                modelo.modificarPersona((Integer)vista.dtm.getValueAt(filaModificada,0),
                        (String)vista.dtm.getValueAt(filaModificada,1), (String)vista.dtm.getValueAt(filaModificada,2),
                        (LocalDate)vista.dtm.getValueAt(filaModificada,3),(String)vista.dtm.getValueAt(filaModificada,4),
                        (String)vista.dtm.getValueAt(filaModificada,5),(Integer)vista.dtm.getValueAt(filaModificada,6));
                vista.lblAccion.setText("Columna Actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }

    }

    /**
     * Metodo iniciarTabla(), inserto los nombres de las columnas de la tabla y las anyado
     */
    private void iniciarTabla(){
        String[] headers = {"id", "nombre", "apellido", "fecha nacimiento", "direccion", "dni", "telefono"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     * Metodo cargarFilas(), el cual lista los datos en su determinada fila y columna
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[7];
        vista.dtm.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);

            vista.dtm.addRow(fila);
        }
        if (resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + " filas cargadas");
        }
    }
}


package com.tlopez.personabbdd;
/**
 * @author Tania
 * @since 13-12-2019
 * Clase Principal
 */
public class Principal {
    /**
     * Metodo main donde creo la clase Vista, Modelo y Controlador
     * @param args
     */
    public static void main(String[] args) {

        Vista vista= new Vista();
        Modelo modelo=new Modelo();
        Controlador controlador=new Controlador(vista,modelo);
    }
}


package com.tlopez.personabbdd;

import java.sql .*;
import java.time.LocalDate;
/**
 * @author Tania
 * @since 13-12-2019
 * Clase Modelo
 */
public class Modelo {
    //Campos
        private Connection conexion;

    /**
     * Metodo conectar(), donde conecto con la base de datos mysql
     * @throws SQLException
     */
    public void conectar() throws SQLException {
            conexion = null;
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/carnet_identidad", "root", "");
        }

    /**
     * Metodo crearTablaPersonas(), que crea una tabla mediante un procedimiento
     * @throws SQLException
     */
    public void crearTablaPersonas() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/carnet_identidad", "root", "");

        String sentenciaSQL="call crearTablaPersona()";
        CallableStatement procedimiento= null;
        procedimiento= conexion.prepareCall(sentenciaSQL);
        procedimiento.execute();

    }

    /**
     * Metodo desconectar(), donde desconecta la base de datos mysql
     * @throws SQLException
     */
        public void desconectar() throws SQLException {
            conexion.close();
            conexion = null;
        }

    /**
     * Metodo crearTabla(), sirve para crear tablas con el nombre que pasas por el parametro
     * @param nombreTabla
     * @return resultado
     * @throws SQLException
     */
        public int crearTabla(String nombreTabla) throws SQLException {
            if (conexion == null)
                return -1;

            if (conexion.isClosed())
                return -2;
            String consulta = "CREATE TABLE IF NOT EXISTS " + nombreTabla + "(id int primary key auto_increment,nombre varchar(50)," +
                    "apellido varchar(50),fecha_nacimiento date,direccion varchar(50),dni varchar(50),telefono int)";

            PreparedStatement sentencia = conexion.prepareStatement(consulta);
            int resultado = sentencia.executeUpdate();

            return resultado;
        }

    /**
     * Metodo obtenerDatos(), sirve para mostrar todos los campos de la tabla personas
     * @return resultado
     * @throws SQLException
     */
        public ResultSet obtenerDatos() throws SQLException {
            if (conexion == null)
                return null;

            if (conexion.isClosed())
                return null;

            String consulta = "SELECT * FROM personas";
            PreparedStatement sentencia = null;

            sentencia = conexion.prepareStatement(consulta);
            ResultSet resultado = sentencia.executeQuery();

            return resultado;
        }

    /**
     * Metodo insertarPersona(), sirve para insertar personas a la tabla persona
     * @param nombre
     * @param apellido
     * @param fechaNacimiento
     * @param direccion
     * @param dni
     * @param telefono
     * @return numeroRegistros
     * @throws SQLException
     */
        public int insertarPersona(String nombre, String apellido, LocalDate fechaNacimiento, String direccion, String dni, int telefono) throws SQLException {
            if (conexion == null)
                return -1;

            if (conexion.isClosed())
                return -2;

            String consulta = "INSERT INTO personas(nombre, apellido, fecha_nacimiento, direccion,dni,telefono)" +
                    "VALUES (?, ?, ?, ? ,? ,?)";
            PreparedStatement sentencia = null;

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setDate(3, Date.valueOf(fechaNacimiento));
            sentencia.setString(4, direccion);
            sentencia.setString(5, dni);
            sentencia.setInt(6, telefono);

            int numeroRegistros = sentencia.executeUpdate();

            if (sentencia != null) {
                sentencia.close();
            }

            return numeroRegistros;
        }

    /**
     * Metodo eliminarPersona(), sirve para eliminar la persona que selecciones de la tabla
     * @param id
     * @return resultado
     * @throws SQLException
     */
        public int eliminarPersona(int id) throws SQLException {
            if (conexion == null)
                return -1;

            if (conexion.isClosed())
                return -2;

            String consulta = "DELETE FROM personas WHERE id = ?";

            PreparedStatement sentencia = null;

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, id);

            int resultado = sentencia.executeUpdate();

            if (sentencia != null) {
                sentencia.close();
            }

            return resultado;

        }

    /**
     * Metodo  vaciarTabla(), que todos los datos de la tabla
     * @return resultado
     * @throws SQLException
     */
    public int vaciarTabla() throws SQLException {
        if (conexion == null)
            return -1;

        if (conexion.isClosed())
            return -2;

        String consulta = "DELETE FROM personas";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }

        return resultado;

    }


    /**
     * Metodo modificarPersona(), sirve para modificar los datos de una persona de la tabla
     * @param id
     * @param nombre
     * @param apellido
     * @param fechaNacimiento
     * @param direccion
     * @param dni
     * @param telefono
     * @return resultado
     * @throws SQLException
     */
        public int modificarPersona(int id, String nombre, String apellido, LocalDate fechaNacimiento, String direccion, String dni, int telefono) throws SQLException {
            if (conexion == null)
                return -1;

            if (conexion.isClosed())
                return -2;

            String consulta = "UPDATE personas SET nombre = ?, apellido = ?, " +
                    "fecha_nacimiento = ?, direccion = ?,dni = ?, telefono = ? WHERE id = ?";
            PreparedStatement sentencia = null;

            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setDate(3, Date.valueOf(fechaNacimiento));
            sentencia.setString(4, direccion);
            sentencia.setString(5, dni);
            sentencia.setInt(6, telefono);
            sentencia.setInt(7, id);

            int resultado = sentencia.executeUpdate();

            if (sentencia == null)
                sentencia.close();

            return resultado;
        }

    /**
     * Metodo buscarDatos(), sirve para buscar cualquier valor de la tabla y que te muestre la fila que contiene ese valor
     * @param valor
     * @return resultado
     * @throws SQLException
     */
        public ResultSet buscarDatos(String valor) throws SQLException {
            if (conexion == null)
                return null;

            if (conexion.isClosed())
                return null;


            String consulta = "SELECT * FROM personas WHERE CONCAT(id,nombre, apellido, fecha_nacimiento,direccion,dni,telefono)" +
                    " LIKE '%" + valor + "%'";
            PreparedStatement sentencia = null;

            sentencia = conexion.prepareStatement(consulta);
            ResultSet resultado = sentencia.executeQuery();

            return resultado;
        }


    }


